#!/usr/bin/env python

# ---- imports -----
import os
from os.path import expanduser
from re import search
from natsort import natsorted

opj = os.path.join

# ----- variables -----
user_home = expanduser("~")
icon_theme_dirs = [
    "/usr/share/icons/",
    "%s/.local/share/icons/" % user_home
]
icon_sizes = [
    "1024", "512", "384", "256", "192", "128", "96",
    "72", "64", "48", "36", "32", "24", "22", "16",
    "scalable", "symbolic"
]
icon_types = [
    "actions", "animations", "apps", "categories",
    "devices", "emblems", "emotes", "international",
    "mimetypes", "places", "status", "stock"
]

# ----- functions -----


def list_installed_themes():
    theme_folders = []
    for path in icon_theme_dirs:
        # theme_folders.append([f.name for f in os.scandir(path) if f.is_dir()])
        for f in os.scandir(path):
            if f.is_dir():
                theme_folders.append(f.name)
    return sorted(theme_folders, key=str.lower)


def get_icon_paths(theme_name, icon_size, icon_type):
    # given the theme name, search for the 'apps' folder at the 48pix size
    icon_paths = []
    for dir in icon_theme_dirs:
        tp = "%s%s" % (dir, theme_name)
        # print(tp)
        for (root, dirs, files) in os.walk(tp):
            if search(f'{icon_type}/{icon_size}$', root):
                # print(root)
                # print(files)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_type}/{icon_size}x{icon_size}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_size}/{icon_type}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_size}x{icon_size}/{icon_type}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
    return icon_paths


def get_icon_paths_for_tabs(theme_name):
    # get the list of dirs and files in the theme folders
    # make a big list[] of all the paths that end in
    # .png or .svg, skip other files
    icon_paths = []
    for dir in icon_theme_dirs:
        # theme path
        tp = f"{dir}{theme_name}/"
        for (root, dirs, files) in os.walk(tp):
            # print(f"dir: {dir}")
            # print(f"root: {root}")
            # print(f"files: {files}")
            for f in files:
                # print(f"icon file: {root}/{f}")
                if not search('^.*.png$', f) and not search('^.*.svg$', f):
                    # print(f)
                    continue
                icon_paths.append(f"{root}/{f}")

    return icon_paths


def sort_icon_paths(paths):
    paths_dict = {}
    sorted_paths = []
    final_paths = {}
    # get a dict and each key is the icon_type and the value
    # is a list of the paths for that icon_type in the theme
    for t in icon_types:
        pl = []
        for p in paths:
            if t in p:
                pl.append(p)
        paths_dict[t] = pl
        pl = []

    # for each icon_type, check if it's value has any content
    # if it has paths, then search the paths for the various icon_sizes
    # add those select paths to the sorted_paths list[]
    for t in icon_types:
        # print(paths_dict[t])
        if len(paths_dict[t]) > 0:
            # print(f"{t}: {len(paths_dict)}")
            # print(sorted(paths_dict[t]))
            for s in icon_sizes:
                for i in sorted(paths_dict[t]):
                    if s in i:
                        # print(i)
                        if search(f'^.*@2x.*$', i):
                            print(f"skipping: {i}")
                            continue
                        else:
                            sorted_paths.append(i)
            # sorted_paths.reverse()
            paths_list = list(set(sorted_paths))
            # for sp in sorted_set:
            #     print(sp)
            paths_list.sort()
            paths_list.reverse()
            final_paths[t] = paths_list
            sorted_paths = []

    return final_paths


def natsort_icon_paths(paths):
    paths_dict = {}
    sorted_paths = []
    final_paths = {}
    # get a dict and each key is the icon_type and the value
    # is a list of the paths for that icon_type in the theme
    for t in icon_types:
        pl = []
        for p in paths:
            # if t in p:
            #     pl.append(p)
            t_str = f"/{t}/"
            if search(t_str, p):
                pl.append(p)
        paths_dict[t] = pl
        pl = []

    # for each icon_type, check if it's value has any content
    # if it has paths, then search the paths for the various icon_sizes
    # add those select paths to the sorted_paths list[]
    for t in icon_types:
        # print(paths_dict[t])
        if len(paths_dict[t]) > 0:
            # print(f"{t}: {len(paths_dict)}")
            # print(natsorted(paths_dict[t]))
            for s in icon_sizes:
                for i in natsorted(paths_dict[t]):
                    # if s in i:
                    # s_str = f"/{s}/"
                    s_str = f"/{s}/|/{s}x{s}/|/{s}@2x/"
                    if search(s_str, i):
                        # print(i)
                        if search(f'^.*@2x.*$', i):
                            continue
                        #     print(f"skipping: {i}")
                        else:
                            sorted_paths.append(i)
            final_paths[t] = sorted_paths
            sorted_paths = []

    return final_paths

# ----- objects -----


# ----- main -----


def main():
    # print(list_installed_themes())
    # print("%s/.local/share/icons/" % user_home)
    # for thm_path in list_installed_themes():
    #     print(get_icon_paths(thm_path))
    # print(get_icon_paths("Arc"))
    aip = get_icon_paths_for_tabs('Adwaita')
    saip = sort_icon_paths(aip)
    # print(saip.keys())
    # for p in saip['devices']:
    #     print(p)


if __name__ == '__main__':
    main()