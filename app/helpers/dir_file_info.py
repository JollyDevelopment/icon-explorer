#!/usr/bin/env python

# ---- imports -----
import os
from os.path import expanduser
from re import search

opj = os.path.join

# ----- variables -----
user_home = expanduser("~")
icon_theme_dirs = [
    "/usr/share/icons/",
    "%s/.local/share/icons/" % user_home
]


# ----- functions -----

def list_installed_themes():
    theme_folders = []
    for path in icon_theme_dirs:
        # theme_folders.append([f.name for f in os.scandir(path) if f.is_dir()])
        for f in os.scandir(path):
            if f.is_dir():
                theme_folders.append(f.name)
    return sorted(theme_folders, key=str.lower)


def get_icon_paths(theme_name, icon_size, icon_type):
    # given the theme name, search for the 'apps' folder at the 48pix size
    icon_paths = []
    for dir in icon_theme_dirs:
        tp = "%s%s" % (dir, theme_name)
        # print(tp)
        for (root, dirs, files) in os.walk(tp):
            if search(f'{icon_type}/{icon_size}$', root):
                # print(root)
                # print(files)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_type}/{icon_size}x{icon_size}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_size}/{icon_type}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
            elif search(f'{icon_size}x{icon_size}/{icon_type}$', root):
                # print(root)
                icon_paths.append(
                    (theme_name, root, files)
                )
    return icon_paths

# ----- objects -----


# ----- main -----
def main():
    print(list_installed_themes())
    # print("%s/.local/share/icons/" % user_home)
    # for thm_path in list_installed_themes():
    #     print(get_icon_paths(thm_path))
    print(get_icon_paths("Arc"))


if __name__ == '__main__':
    main()